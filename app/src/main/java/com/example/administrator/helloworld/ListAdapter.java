package com.example.administrator.helloworld;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016-12-11.
 */



public class ListAdapter extends BaseAdapter {
   private Context mContext;
     List<BoardVo> data = new ArrayList<BoardVo>();

    public ListAdapter (Context context){
        mContext=context;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View v, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_board_row , null);


        TextView tv1= (TextView)view.findViewById(R.id.name);
        TextView tv2= (TextView)view.findViewById(R.id.content);

        return view;
    }

}

