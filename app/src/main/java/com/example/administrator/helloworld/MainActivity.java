package com.example.administrator.helloworld;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private ListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new ListAdapter(this);

        listView = (ListView)findViewById(R.id.board_list);
        listView.setAdapter(adapter);

        BoardVo vo = new BoardVo();
        vo.setContent("내용");
        vo.setName("이름");
        adapter.data.add(vo);




    }
    }




