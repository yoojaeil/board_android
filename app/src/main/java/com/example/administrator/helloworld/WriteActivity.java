package com.example.administrator.helloworld;

import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by Administrator on 2016-12-10.
 */

public class WriteActivity {

    private void save(String name, String content) {
        try {
            String url = "http://default-environment.jwgwrggxca.ap-northeast-2.elasticbeanstalk.com/write";

            JSONObject obj = new JSONObject();
            obj.put("name", name);
            obj.put("content", content);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, url, obj,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(WriteActivity.this, "success", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    },

                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(WriteActivity.this, "fail", Toast.LENGTH_SHORT).show();

                            error.printStackTrace();
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(jsObjRequest);

        } catch (Exception e) {

        }
    }

}
